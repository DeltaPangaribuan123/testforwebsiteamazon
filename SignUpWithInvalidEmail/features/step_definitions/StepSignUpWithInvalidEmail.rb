require 'selenium-webdriver'
require 'rubygems'
require 'rspec'

driver = Selenium::WebDriver.for:chrome 
wait = Selenium::WebDriver::Wait.new(:timeout => 5) 

Given('I launch website') do
    driver.navigate.to "https://www.amazon.com/"
  end
  
  Given('I see Hello, Sign in Account Menu') do
    driver.find_element(:class, "nav-line-1")
  end
  
  Given('I click Start here hyperlink') do
    driver.find_element(:link, "Start here.").click
  end

  Given('I enter name') do
    input = wait.until {
    element = driver.find_element(:name, "customerName")
    element if element.displayed?
    }
    input.send_keys("Delta Pangaribuan")
  end
  
  Given('I enter invalid email') do
    input = wait.until {
        element = driver.find_element(:name, "email")
        element if element.displayed?
        }
        input.send_keys("delta.com")
  end
  
  Given('I enter password') do
    input = wait.until {
        element = driver.find_element(:name, "password")
        element if element.displayed?
        }
        input.send_keys("delta123")
  end
  
  Given('I enter re-enter password') do
    input = wait.until {
        element = driver.find_element(:name, "passwordCheck")
        element if element.displayed?
        }
        input.send_keys("delta123")
  end
  
  When('I clicked Create your Account Amazon') do
    driver.find_element(:id, "a-autoid-0").click
  end
  
  Then('I see alert Enter a valid email address') do
    driver.find_element(:class, "a-alert-content")
  end