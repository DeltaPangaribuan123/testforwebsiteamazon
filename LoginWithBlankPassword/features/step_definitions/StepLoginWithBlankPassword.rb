require 'selenium-webdriver'
require 'rubygems'
require 'rspec'

driver = Selenium::WebDriver.for:chrome 
wait = Selenium::WebDriver::Wait.new(:timeout => 5) 

  Given('I launch website') do    
    driver.navigate.to "https://www.amazon.com/"
  end
  
  Given('I see Hello, Sign in Account Menu') do
    driver.find_element(:class, "nav-line-1")
  end
  Given('I click Sign in button') do
    driver.find_element(:link, "Sign in").click
  end
  
  Given('I enter Email or mobile phone number') do
    input = wait.until {
        element = driver.find_element(:name, "email")
        element if element.displayed?
        }
        input.send_keys("deltaflorentina123@gmail.com")
  end
  
  When('I click Continue') do
    driver.find_element(:id, "continue").click
  end
   
    Then('I click Sign-in button') do
        driver.find_element(:xpath, '//*[@id="signInSubmit"]').click
      end

   Then('I see alert Enter your password') do
    driver.find_element(:xpath, '//*[@id="auth-password-missing-alert"]/div/div')
    end


  