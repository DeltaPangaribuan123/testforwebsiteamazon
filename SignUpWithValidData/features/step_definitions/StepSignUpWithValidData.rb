require 'selenium-webdriver'
require 'rubygems'
require 'rspec'

driver = Selenium::WebDriver.for:chrome 
wait = Selenium::WebDriver::Wait.new(:timeout => 5) 

Given('I launch website') do
    driver.navigate.to "https://www.amazon.com/"
  end
  
  Given('I see Hello, Sign in Account Menu') do
    driver.find_element(:class, "nav-line-1")
  end
  
  Given('I click Start here hyperlink') do
    driver.find_element(:link, "Start here.").click
  end

  Given('I enter name') do
    input = wait.until {
    element = driver.find_element(:name, "customerName")
    element if element.displayed?
    }
    input.send_keys("Delta Pangaribuan")
  end
  
  Given('I enter email') do
    input = wait.until {
        element = driver.find_element(:name, "email")
        element if element.displayed?
        }
        input.send_keys("deltaflorentin123@gmail.com")
  end
  
  Given('I enter password') do
    input = wait.until {
        element = driver.find_element(:name, "password")
        element if element.displayed?
        }
        input.send_keys("delta123")
  end
  
  Given('I enter re-enter password') do
    input = wait.until {
        element = driver.find_element(:name, "passwordCheck")
        element if element.displayed?
        }
        input.send_keys("delta123")
  end
  
  When('I clicked Create your Account Amazon') do
    driver.find_element(:id, "a-autoid-0").click
  end
  
  Then('I see Verify Email Adress page') do
    driver.navigate.to "https://www.amazon.com/ap/cvf/request?arb=3397da9f-d226-4ed5-9380-f14cfb0daf22"
  end
  
  Then('I enter OTP') do
    input = wait.until {
        element = driver.find_element(:name, "code")
        element if element.displayed?
        }
        input.send_keys("924978")
  end
  
  When('I clicked Create your Amazon account') do
    driver.find_element(:id, "a-autoid-0").click
  end
  
  Then('I see Home page') do
    driver.navigate.to "https://www.amazon.com/?_encoding=UTF8&ref_=nav_newcust&"
  end